// Fill out your copyright notice in the Description page of Project Settings.


#include "PatrolAIController.h"
#include "PatrolAI.h"

void APatrolAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	APatrolAI* controller = Cast<APatrolAI>(GetCharacter());
	controller->MoveToNext();
}
