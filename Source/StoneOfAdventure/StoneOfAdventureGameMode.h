// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "StoneOfAdventureGameMode.generated.h"

UCLASS(minimalapi)
class AStoneOfAdventureGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AStoneOfAdventureGameMode();
};



