// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PatrolAI.generated.h"

UCLASS()
class STONEOFADVENTURE_API APatrolAI : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APatrolAI();

	UPROPERTY(VisibleAnywhere, BlueprintReadonly, Category = "AI")
	class AAIController* AIController;

	UPROPERTY(EditAnywhere, BlueprintReadonly, Category = "Movement")
	TArray<class AAIWayPoint*> WayPoints;

	UPROPERTY(EditAnywhere, BlueprintReadonly, Category = "Movement")
	AAIWayPoint* TargetWP;

	UPROPERTY(EditAnywhere, BlueprintReadonly, Category = "Movement")
	int CurrentWPIndex;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void MoveToNext();
};
