// Fill out your copyright notice in the Description page of Project Settings.


#include "PatrolAI.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/SphereComponent.h"
#include "AITypes.h"
#include "AIWayPoint.h"
#include "AIController.h"
#include "Containers/Array.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
APatrolAI::APatrolAI()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CurrentWPIndex = -1;
	GetCharacterMovement()->MaxWalkSpeed = 170.f;
}

// Called when the game starts or when spawned
void APatrolAI::BeginPlay()
{
	Super::BeginPlay();
	
	AIController = Cast<AAIController>(GetController());

	MoveToNext();
}

// Called every frame
void APatrolAI::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


}

// Called to bind functionality to input
void APatrolAI::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void APatrolAI::MoveToNext()
{
	if (WayPoints.Num() == 0)
	{
		return;
	}

	CurrentWPIndex++;
	if (CurrentWPIndex >= WayPoints.Num())
	{
		CurrentWPIndex = 0;
	}

	TargetWP = WayPoints[CurrentWPIndex];
	if (!TargetWP)
	{
		return;
	}

	FAIMoveRequest moveRequest;	
	moveRequest.SetGoalActor(TargetWP);
	moveRequest.SetAcceptanceRadius(1.f);

	FNavPathSharedPtr path;

	AIController->MoveTo(moveRequest, &path);
}
