// Copyright Epic Games, Inc. All Rights Reserved.

#include "StoneOfAdventureGameMode.h"
#include "StoneOfAdventureHUD.h"
#include "StoneOfAdventureCharacter.h"
#include "UObject/ConstructorHelpers.h"

AStoneOfAdventureGameMode::AStoneOfAdventureGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AStoneOfAdventureHUD::StaticClass();
}
