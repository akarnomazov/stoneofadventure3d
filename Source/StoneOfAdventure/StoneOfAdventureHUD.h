// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "StoneOfAdventureHUD.generated.h"

UCLASS()
class AStoneOfAdventureHUD : public AHUD
{
	GENERATED_BODY()

public:
	AStoneOfAdventureHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

