// Fill out your copyright notice in the Description page of Project Settings.


#include "AIWayPoint.h"
#include "Components/SphereComponent.h"

// Sets default values
AAIWayPoint::AAIWayPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionVolume = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionVolume"));
}

// Called when the game starts or when spawned
void AAIWayPoint::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAIWayPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAIWayPoint::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

}

void AAIWayPoint::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

}
