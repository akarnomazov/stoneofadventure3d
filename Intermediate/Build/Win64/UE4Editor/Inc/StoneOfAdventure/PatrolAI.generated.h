// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STONEOFADVENTURE_PatrolAI_generated_h
#error "PatrolAI.generated.h already included, missing '#pragma once' in PatrolAI.h"
#endif
#define STONEOFADVENTURE_PatrolAI_generated_h

#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_12_SPARSE_DATA
#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_12_RPC_WRAPPERS
#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPatrolAI(); \
	friend struct Z_Construct_UClass_APatrolAI_Statics; \
public: \
	DECLARE_CLASS(APatrolAI, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/StoneOfAdventure"), NO_API) \
	DECLARE_SERIALIZER(APatrolAI)


#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPatrolAI(); \
	friend struct Z_Construct_UClass_APatrolAI_Statics; \
public: \
	DECLARE_CLASS(APatrolAI, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/StoneOfAdventure"), NO_API) \
	DECLARE_SERIALIZER(APatrolAI)


#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APatrolAI(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APatrolAI) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APatrolAI); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APatrolAI); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APatrolAI(APatrolAI&&); \
	NO_API APatrolAI(const APatrolAI&); \
public:


#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APatrolAI(APatrolAI&&); \
	NO_API APatrolAI(const APatrolAI&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APatrolAI); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APatrolAI); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APatrolAI)


#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_12_PRIVATE_PROPERTY_OFFSET
#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_9_PROLOG
#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_12_PRIVATE_PROPERTY_OFFSET \
	stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_12_SPARSE_DATA \
	stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_12_RPC_WRAPPERS \
	stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_12_INCLASS \
	stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_12_PRIVATE_PROPERTY_OFFSET \
	stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_12_SPARSE_DATA \
	stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_12_INCLASS_NO_PURE_DECLS \
	stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STONEOFADVENTURE_API UClass* StaticClass<class APatrolAI>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID stoneofadventure3d_Source_StoneOfAdventure_PatrolAI_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
