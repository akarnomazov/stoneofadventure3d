// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StoneOfAdventure/PatrolAI.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePatrolAI() {}
// Cross Module References
	STONEOFADVENTURE_API UClass* Z_Construct_UClass_APatrolAI_NoRegister();
	STONEOFADVENTURE_API UClass* Z_Construct_UClass_APatrolAI();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_StoneOfAdventure();
	AIMODULE_API UClass* Z_Construct_UClass_AAIController_NoRegister();
	STONEOFADVENTURE_API UClass* Z_Construct_UClass_AAIWayPoint_NoRegister();
// End Cross Module References
	void APatrolAI::StaticRegisterNativesAPatrolAI()
	{
	}
	UClass* Z_Construct_UClass_APatrolAI_NoRegister()
	{
		return APatrolAI::StaticClass();
	}
	struct Z_Construct_UClass_APatrolAI_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AIController_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AIController;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WayPoints_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WayPoints_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_WayPoints;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetWP_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TargetWP;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentWPIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_CurrentWPIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APatrolAI_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_StoneOfAdventure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APatrolAI_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "PatrolAI.h" },
		{ "ModuleRelativePath", "PatrolAI.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APatrolAI_Statics::NewProp_AIController_MetaData[] = {
		{ "Category", "AI" },
		{ "ModuleRelativePath", "PatrolAI.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APatrolAI_Statics::NewProp_AIController = { "AIController", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APatrolAI, AIController), Z_Construct_UClass_AAIController_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APatrolAI_Statics::NewProp_AIController_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APatrolAI_Statics::NewProp_AIController_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APatrolAI_Statics::NewProp_WayPoints_Inner = { "WayPoints", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AAIWayPoint_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APatrolAI_Statics::NewProp_WayPoints_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "PatrolAI.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_APatrolAI_Statics::NewProp_WayPoints = { "WayPoints", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APatrolAI, WayPoints), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_APatrolAI_Statics::NewProp_WayPoints_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APatrolAI_Statics::NewProp_WayPoints_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APatrolAI_Statics::NewProp_TargetWP_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "PatrolAI.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APatrolAI_Statics::NewProp_TargetWP = { "TargetWP", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APatrolAI, TargetWP), Z_Construct_UClass_AAIWayPoint_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APatrolAI_Statics::NewProp_TargetWP_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APatrolAI_Statics::NewProp_TargetWP_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APatrolAI_Statics::NewProp_CurrentWPIndex_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "PatrolAI.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_APatrolAI_Statics::NewProp_CurrentWPIndex = { "CurrentWPIndex", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APatrolAI, CurrentWPIndex), METADATA_PARAMS(Z_Construct_UClass_APatrolAI_Statics::NewProp_CurrentWPIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APatrolAI_Statics::NewProp_CurrentWPIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APatrolAI_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APatrolAI_Statics::NewProp_AIController,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APatrolAI_Statics::NewProp_WayPoints_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APatrolAI_Statics::NewProp_WayPoints,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APatrolAI_Statics::NewProp_TargetWP,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APatrolAI_Statics::NewProp_CurrentWPIndex,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APatrolAI_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APatrolAI>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APatrolAI_Statics::ClassParams = {
		&APatrolAI::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_APatrolAI_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_APatrolAI_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_APatrolAI_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APatrolAI_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APatrolAI()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APatrolAI_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APatrolAI, 3212120573);
	template<> STONEOFADVENTURE_API UClass* StaticClass<APatrolAI>()
	{
		return APatrolAI::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APatrolAI(Z_Construct_UClass_APatrolAI, &APatrolAI::StaticClass, TEXT("/Script/StoneOfAdventure"), TEXT("APatrolAI"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APatrolAI);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
