// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STONEOFADVENTURE_PatrolAIController_generated_h
#error "PatrolAIController.generated.h already included, missing '#pragma once' in PatrolAIController.h"
#endif
#define STONEOFADVENTURE_PatrolAIController_generated_h

#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_15_SPARSE_DATA
#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_15_RPC_WRAPPERS
#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPatrolAIController(); \
	friend struct Z_Construct_UClass_APatrolAIController_Statics; \
public: \
	DECLARE_CLASS(APatrolAIController, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/StoneOfAdventure"), NO_API) \
	DECLARE_SERIALIZER(APatrolAIController)


#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPatrolAIController(); \
	friend struct Z_Construct_UClass_APatrolAIController_Statics; \
public: \
	DECLARE_CLASS(APatrolAIController, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/StoneOfAdventure"), NO_API) \
	DECLARE_SERIALIZER(APatrolAIController)


#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APatrolAIController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APatrolAIController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APatrolAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APatrolAIController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APatrolAIController(APatrolAIController&&); \
	NO_API APatrolAIController(const APatrolAIController&); \
public:


#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APatrolAIController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APatrolAIController(APatrolAIController&&); \
	NO_API APatrolAIController(const APatrolAIController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APatrolAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APatrolAIController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APatrolAIController)


#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_15_PRIVATE_PROPERTY_OFFSET
#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_12_PROLOG
#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_15_PRIVATE_PROPERTY_OFFSET \
	stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_15_SPARSE_DATA \
	stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_15_RPC_WRAPPERS \
	stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_15_INCLASS \
	stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_15_PRIVATE_PROPERTY_OFFSET \
	stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_15_SPARSE_DATA \
	stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_15_INCLASS_NO_PURE_DECLS \
	stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STONEOFADVENTURE_API UClass* StaticClass<class APatrolAIController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID stoneofadventure3d_Source_StoneOfAdventure_PatrolAIController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
