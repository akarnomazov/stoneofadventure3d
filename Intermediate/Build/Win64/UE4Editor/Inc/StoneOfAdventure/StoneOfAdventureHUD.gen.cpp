// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StoneOfAdventure/StoneOfAdventureHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeStoneOfAdventureHUD() {}
// Cross Module References
	STONEOFADVENTURE_API UClass* Z_Construct_UClass_AStoneOfAdventureHUD_NoRegister();
	STONEOFADVENTURE_API UClass* Z_Construct_UClass_AStoneOfAdventureHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_StoneOfAdventure();
// End Cross Module References
	void AStoneOfAdventureHUD::StaticRegisterNativesAStoneOfAdventureHUD()
	{
	}
	UClass* Z_Construct_UClass_AStoneOfAdventureHUD_NoRegister()
	{
		return AStoneOfAdventureHUD::StaticClass();
	}
	struct Z_Construct_UClass_AStoneOfAdventureHUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AStoneOfAdventureHUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_StoneOfAdventure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AStoneOfAdventureHUD_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "StoneOfAdventureHUD.h" },
		{ "ModuleRelativePath", "StoneOfAdventureHUD.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AStoneOfAdventureHUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AStoneOfAdventureHUD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AStoneOfAdventureHUD_Statics::ClassParams = {
		&AStoneOfAdventureHUD::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AStoneOfAdventureHUD_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AStoneOfAdventureHUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AStoneOfAdventureHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AStoneOfAdventureHUD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AStoneOfAdventureHUD, 4130875721);
	template<> STONEOFADVENTURE_API UClass* StaticClass<AStoneOfAdventureHUD>()
	{
		return AStoneOfAdventureHUD::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AStoneOfAdventureHUD(Z_Construct_UClass_AStoneOfAdventureHUD, &AStoneOfAdventureHUD::StaticClass, TEXT("/Script/StoneOfAdventure"), TEXT("AStoneOfAdventureHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AStoneOfAdventureHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
