// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef STONEOFADVENTURE_AIWayPoint_generated_h
#error "AIWayPoint.generated.h already included, missing '#pragma once' in AIWayPoint.h"
#endif
#define STONEOFADVENTURE_AIWayPoint_generated_h

#define stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_12_SPARSE_DATA
#define stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnOverlapEnd); \
	DECLARE_FUNCTION(execOnOverlapBegin);


#define stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnOverlapEnd); \
	DECLARE_FUNCTION(execOnOverlapBegin);


#define stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAIWayPoint(); \
	friend struct Z_Construct_UClass_AAIWayPoint_Statics; \
public: \
	DECLARE_CLASS(AAIWayPoint, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/StoneOfAdventure"), NO_API) \
	DECLARE_SERIALIZER(AAIWayPoint)


#define stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAAIWayPoint(); \
	friend struct Z_Construct_UClass_AAIWayPoint_Statics; \
public: \
	DECLARE_CLASS(AAIWayPoint, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/StoneOfAdventure"), NO_API) \
	DECLARE_SERIALIZER(AAIWayPoint)


#define stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAIWayPoint(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAIWayPoint) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAIWayPoint); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAIWayPoint); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAIWayPoint(AAIWayPoint&&); \
	NO_API AAIWayPoint(const AAIWayPoint&); \
public:


#define stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAIWayPoint(AAIWayPoint&&); \
	NO_API AAIWayPoint(const AAIWayPoint&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAIWayPoint); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAIWayPoint); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAIWayPoint)


#define stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_12_PRIVATE_PROPERTY_OFFSET
#define stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_9_PROLOG
#define stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_12_PRIVATE_PROPERTY_OFFSET \
	stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_12_SPARSE_DATA \
	stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_12_RPC_WRAPPERS \
	stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_12_INCLASS \
	stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_12_PRIVATE_PROPERTY_OFFSET \
	stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_12_SPARSE_DATA \
	stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_12_INCLASS_NO_PURE_DECLS \
	stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STONEOFADVENTURE_API UClass* StaticClass<class AAIWayPoint>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID stoneofadventure3d_Source_StoneOfAdventure_AIWayPoint_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
