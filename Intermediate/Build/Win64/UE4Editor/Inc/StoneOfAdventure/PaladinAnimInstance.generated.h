// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STONEOFADVENTURE_PaladinAnimInstance_generated_h
#error "PaladinAnimInstance.generated.h already included, missing '#pragma once' in PaladinAnimInstance.h"
#endif
#define STONEOFADVENTURE_PaladinAnimInstance_generated_h

#define stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_15_SPARSE_DATA
#define stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execUpdateAnimationProperties);


#define stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execUpdateAnimationProperties);


#define stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPaladinAnimInstance(); \
	friend struct Z_Construct_UClass_UPaladinAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UPaladinAnimInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/StoneOfAdventure"), NO_API) \
	DECLARE_SERIALIZER(UPaladinAnimInstance)


#define stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUPaladinAnimInstance(); \
	friend struct Z_Construct_UClass_UPaladinAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UPaladinAnimInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/StoneOfAdventure"), NO_API) \
	DECLARE_SERIALIZER(UPaladinAnimInstance)


#define stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaladinAnimInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaladinAnimInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaladinAnimInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaladinAnimInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaladinAnimInstance(UPaladinAnimInstance&&); \
	NO_API UPaladinAnimInstance(const UPaladinAnimInstance&); \
public:


#define stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaladinAnimInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaladinAnimInstance(UPaladinAnimInstance&&); \
	NO_API UPaladinAnimInstance(const UPaladinAnimInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaladinAnimInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaladinAnimInstance); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaladinAnimInstance)


#define stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_15_PRIVATE_PROPERTY_OFFSET
#define stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_12_PROLOG
#define stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_15_PRIVATE_PROPERTY_OFFSET \
	stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_15_SPARSE_DATA \
	stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_15_RPC_WRAPPERS \
	stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_15_INCLASS \
	stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_15_PRIVATE_PROPERTY_OFFSET \
	stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_15_SPARSE_DATA \
	stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_15_INCLASS_NO_PURE_DECLS \
	stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STONEOFADVENTURE_API UClass* StaticClass<class UPaladinAnimInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID stoneofadventure3d_Source_StoneOfAdventure_PaladinAnimInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
