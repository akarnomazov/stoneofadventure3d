// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "StoneOfAdventure/StoneOfAdventureGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeStoneOfAdventureGameMode() {}
// Cross Module References
	STONEOFADVENTURE_API UClass* Z_Construct_UClass_AStoneOfAdventureGameMode_NoRegister();
	STONEOFADVENTURE_API UClass* Z_Construct_UClass_AStoneOfAdventureGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_StoneOfAdventure();
// End Cross Module References
	void AStoneOfAdventureGameMode::StaticRegisterNativesAStoneOfAdventureGameMode()
	{
	}
	UClass* Z_Construct_UClass_AStoneOfAdventureGameMode_NoRegister()
	{
		return AStoneOfAdventureGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AStoneOfAdventureGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AStoneOfAdventureGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_StoneOfAdventure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AStoneOfAdventureGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "StoneOfAdventureGameMode.h" },
		{ "ModuleRelativePath", "StoneOfAdventureGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AStoneOfAdventureGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AStoneOfAdventureGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AStoneOfAdventureGameMode_Statics::ClassParams = {
		&AStoneOfAdventureGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AStoneOfAdventureGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AStoneOfAdventureGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AStoneOfAdventureGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AStoneOfAdventureGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AStoneOfAdventureGameMode, 2052071037);
	template<> STONEOFADVENTURE_API UClass* StaticClass<AStoneOfAdventureGameMode>()
	{
		return AStoneOfAdventureGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AStoneOfAdventureGameMode(Z_Construct_UClass_AStoneOfAdventureGameMode, &AStoneOfAdventureGameMode::StaticClass, TEXT("/Script/StoneOfAdventure"), TEXT("AStoneOfAdventureGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AStoneOfAdventureGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
