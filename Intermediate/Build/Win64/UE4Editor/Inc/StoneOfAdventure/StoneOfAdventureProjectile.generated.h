// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef STONEOFADVENTURE_StoneOfAdventureProjectile_generated_h
#error "StoneOfAdventureProjectile.generated.h already included, missing '#pragma once' in StoneOfAdventureProjectile.h"
#endif
#define STONEOFADVENTURE_StoneOfAdventureProjectile_generated_h

#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_15_SPARSE_DATA
#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit);


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit);


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAStoneOfAdventureProjectile(); \
	friend struct Z_Construct_UClass_AStoneOfAdventureProjectile_Statics; \
public: \
	DECLARE_CLASS(AStoneOfAdventureProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/StoneOfAdventure"), NO_API) \
	DECLARE_SERIALIZER(AStoneOfAdventureProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAStoneOfAdventureProjectile(); \
	friend struct Z_Construct_UClass_AStoneOfAdventureProjectile_Statics; \
public: \
	DECLARE_CLASS(AStoneOfAdventureProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/StoneOfAdventure"), NO_API) \
	DECLARE_SERIALIZER(AStoneOfAdventureProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AStoneOfAdventureProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AStoneOfAdventureProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStoneOfAdventureProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStoneOfAdventureProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStoneOfAdventureProjectile(AStoneOfAdventureProjectile&&); \
	NO_API AStoneOfAdventureProjectile(const AStoneOfAdventureProjectile&); \
public:


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStoneOfAdventureProjectile(AStoneOfAdventureProjectile&&); \
	NO_API AStoneOfAdventureProjectile(const AStoneOfAdventureProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStoneOfAdventureProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStoneOfAdventureProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AStoneOfAdventureProjectile)


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(AStoneOfAdventureProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(AStoneOfAdventureProjectile, ProjectileMovement); }


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_12_PROLOG
#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_15_PRIVATE_PROPERTY_OFFSET \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_15_SPARSE_DATA \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_15_RPC_WRAPPERS \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_15_INCLASS \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_15_PRIVATE_PROPERTY_OFFSET \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_15_SPARSE_DATA \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_15_INCLASS_NO_PURE_DECLS \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STONEOFADVENTURE_API UClass* StaticClass<class AStoneOfAdventureProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
