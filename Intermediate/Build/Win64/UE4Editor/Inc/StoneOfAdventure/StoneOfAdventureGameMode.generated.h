// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STONEOFADVENTURE_StoneOfAdventureGameMode_generated_h
#error "StoneOfAdventureGameMode.generated.h already included, missing '#pragma once' in StoneOfAdventureGameMode.h"
#endif
#define STONEOFADVENTURE_StoneOfAdventureGameMode_generated_h

#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_12_SPARSE_DATA
#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_12_RPC_WRAPPERS
#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAStoneOfAdventureGameMode(); \
	friend struct Z_Construct_UClass_AStoneOfAdventureGameMode_Statics; \
public: \
	DECLARE_CLASS(AStoneOfAdventureGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/StoneOfAdventure"), STONEOFADVENTURE_API) \
	DECLARE_SERIALIZER(AStoneOfAdventureGameMode)


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAStoneOfAdventureGameMode(); \
	friend struct Z_Construct_UClass_AStoneOfAdventureGameMode_Statics; \
public: \
	DECLARE_CLASS(AStoneOfAdventureGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/StoneOfAdventure"), STONEOFADVENTURE_API) \
	DECLARE_SERIALIZER(AStoneOfAdventureGameMode)


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	STONEOFADVENTURE_API AStoneOfAdventureGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AStoneOfAdventureGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(STONEOFADVENTURE_API, AStoneOfAdventureGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStoneOfAdventureGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	STONEOFADVENTURE_API AStoneOfAdventureGameMode(AStoneOfAdventureGameMode&&); \
	STONEOFADVENTURE_API AStoneOfAdventureGameMode(const AStoneOfAdventureGameMode&); \
public:


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	STONEOFADVENTURE_API AStoneOfAdventureGameMode(AStoneOfAdventureGameMode&&); \
	STONEOFADVENTURE_API AStoneOfAdventureGameMode(const AStoneOfAdventureGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(STONEOFADVENTURE_API, AStoneOfAdventureGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStoneOfAdventureGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AStoneOfAdventureGameMode)


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_9_PROLOG
#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_12_SPARSE_DATA \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_12_RPC_WRAPPERS \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_12_INCLASS \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_12_SPARSE_DATA \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_12_INCLASS_NO_PURE_DECLS \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STONEOFADVENTURE_API UClass* StaticClass<class AStoneOfAdventureGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
