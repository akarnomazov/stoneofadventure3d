// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STONEOFADVENTURE_StoneOfAdventureHUD_generated_h
#error "StoneOfAdventureHUD.generated.h already included, missing '#pragma once' in StoneOfAdventureHUD.h"
#endif
#define STONEOFADVENTURE_StoneOfAdventureHUD_generated_h

#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_12_SPARSE_DATA
#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_12_RPC_WRAPPERS
#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAStoneOfAdventureHUD(); \
	friend struct Z_Construct_UClass_AStoneOfAdventureHUD_Statics; \
public: \
	DECLARE_CLASS(AStoneOfAdventureHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/StoneOfAdventure"), NO_API) \
	DECLARE_SERIALIZER(AStoneOfAdventureHUD)


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAStoneOfAdventureHUD(); \
	friend struct Z_Construct_UClass_AStoneOfAdventureHUD_Statics; \
public: \
	DECLARE_CLASS(AStoneOfAdventureHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/StoneOfAdventure"), NO_API) \
	DECLARE_SERIALIZER(AStoneOfAdventureHUD)


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AStoneOfAdventureHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AStoneOfAdventureHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStoneOfAdventureHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStoneOfAdventureHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStoneOfAdventureHUD(AStoneOfAdventureHUD&&); \
	NO_API AStoneOfAdventureHUD(const AStoneOfAdventureHUD&); \
public:


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStoneOfAdventureHUD(AStoneOfAdventureHUD&&); \
	NO_API AStoneOfAdventureHUD(const AStoneOfAdventureHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStoneOfAdventureHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStoneOfAdventureHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AStoneOfAdventureHUD)


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_9_PROLOG
#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_12_SPARSE_DATA \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_12_RPC_WRAPPERS \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_12_INCLASS \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_12_SPARSE_DATA \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_12_INCLASS_NO_PURE_DECLS \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STONEOFADVENTURE_API UClass* StaticClass<class AStoneOfAdventureHUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
