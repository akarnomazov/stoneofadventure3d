// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STONEOFADVENTURE_StoneOfAdventureCharacter_generated_h
#error "StoneOfAdventureCharacter.generated.h already included, missing '#pragma once' in StoneOfAdventureCharacter.h"
#endif
#define STONEOFADVENTURE_StoneOfAdventureCharacter_generated_h

#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_20_SPARSE_DATA
#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_20_RPC_WRAPPERS
#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAStoneOfAdventureCharacter(); \
	friend struct Z_Construct_UClass_AStoneOfAdventureCharacter_Statics; \
public: \
	DECLARE_CLASS(AStoneOfAdventureCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/StoneOfAdventure"), NO_API) \
	DECLARE_SERIALIZER(AStoneOfAdventureCharacter)


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_20_INCLASS \
private: \
	static void StaticRegisterNativesAStoneOfAdventureCharacter(); \
	friend struct Z_Construct_UClass_AStoneOfAdventureCharacter_Statics; \
public: \
	DECLARE_CLASS(AStoneOfAdventureCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/StoneOfAdventure"), NO_API) \
	DECLARE_SERIALIZER(AStoneOfAdventureCharacter)


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AStoneOfAdventureCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AStoneOfAdventureCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStoneOfAdventureCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStoneOfAdventureCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStoneOfAdventureCharacter(AStoneOfAdventureCharacter&&); \
	NO_API AStoneOfAdventureCharacter(const AStoneOfAdventureCharacter&); \
public:


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStoneOfAdventureCharacter(AStoneOfAdventureCharacter&&); \
	NO_API AStoneOfAdventureCharacter(const AStoneOfAdventureCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStoneOfAdventureCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStoneOfAdventureCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AStoneOfAdventureCharacter)


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(AStoneOfAdventureCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(AStoneOfAdventureCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(AStoneOfAdventureCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(AStoneOfAdventureCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(AStoneOfAdventureCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(AStoneOfAdventureCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(AStoneOfAdventureCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(AStoneOfAdventureCharacter, L_MotionController); }


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_17_PROLOG
#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_20_PRIVATE_PROPERTY_OFFSET \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_20_SPARSE_DATA \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_20_RPC_WRAPPERS \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_20_INCLASS \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_20_PRIVATE_PROPERTY_OFFSET \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_20_SPARSE_DATA \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_20_INCLASS_NO_PURE_DECLS \
	stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STONEOFADVENTURE_API UClass* StaticClass<class AStoneOfAdventureCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID stoneofadventure3d_Source_StoneOfAdventure_StoneOfAdventureCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
